Dolores Tuna is the leading brand of canned tuna nationwide, thanks to its more than 30 years of experience and its high quality standards.

Dolores Tuna pampers the most demanding palates of its consumers and seeks continuous improvements to be on the tables of all homes in Mexico.
